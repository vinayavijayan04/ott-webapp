import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OttResult } from './model/ott-result';

@Injectable({
  providedIn: 'root'
})
export class OttService {

  apiUrl = "https://outtvapp.ottxperience.com/Backend.svc/getallfilteredmetadata";

  constructor(private http:HttpClient) { }

  getOttMetadata(){
    return this.http.get<OttResult>(this.apiUrl);
  }
}
