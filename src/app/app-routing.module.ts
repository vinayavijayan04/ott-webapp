import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OverviewComponent } from './overview/overview.component';
import { ExtraComponent } from './extra/extra.component';
import { HelpComponent } from './help/help.component';

const routes: Routes = [
	{path:'extra',component:ExtraComponent},
	{path:'overview',component:OverviewComponent},
	{path:'help',component:HelpComponent},
	{path:'', redirectTo:'overview',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
