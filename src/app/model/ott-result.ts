import { OttMetadata } from './ott-metadata';

export class OttResult{
	MetaData:Array<OttMetadata>;
	PerformanceCost:number;
	ResultMessage:string;
	ServerTime:string;
	SessionId:string;
	StatusCode:number;
	Success:boolean;
}