import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { from } from 'rxjs';
import { mergeMap, groupBy, toArray } from 'rxjs/operators';
import { OttService } from '../ott.service';
import { OttResult } from '../model/ott-result';
import { OttMetadata} from '../model/ott-metadata';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {

  ottMetadata: Array<OttMetadata>;
  ottResult: OttResult;
  serieMetadata: Array<OttMetadata>;
  flimMetadata: Array<OttMetadata>;
  showMetadata: Array<OttMetadata>;
  documentaryMetadata: Array<OttMetadata>;
  unknownMetadata: Array<OttMetadata>;

  constructor(caroconfig: NgbCarouselConfig, private ottService: OttService) {
  }

  ngOnInit(): void {
    this.ottService.getOttMetadata().subscribe(response => {
      this.ottResult = response;
      this.ottMetadata = this.ottResult.MetaData;
      this.getGroupedObjects(this.ottMetadata);
    });
  }


  getGroupedObjects(otts: Array<OttMetadata>) {
    const source = from(otts);
    const observObj = source.pipe(groupBy(ott => ott.ContentType), mergeMap(ot => ot.pipe(toArray())));
    observObj.subscribe(val => {
      switch (val[0].ContentType) {
        case 'Serie':
          this.serieMetadata = val;
          break;
        case 'Film':
          this.flimMetadata = val;
          break;
        case 'Show':
          this.showMetadata = val;
          break;
        case 'Documentary':
          this.documentaryMetadata = val;
          break;
        default:
          this.unknownMetadata = val;
          break;
      }
    });
  }

  carouselOptions = {
    margin: 25,
    nav: true,
   // navText: ["<span class='glyphicon glyphicon-menu-left'></span>", "<div class='nav-btn next-slide'></div>"],
   // navText: ["<img src='./assets/overview/right.png'>", "<img src='./assets/overview/right.png'>"],
    responsiveClass: true,
    dots: false,
    navElement : 'div',
    navContainer : false,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: true
      },
      1000: {
        items: 2,
        nav: true,
        loop: false
      },
      1500: {
        items: 3,
        nav: true,
        loop: false
      }
    }
  }

}
